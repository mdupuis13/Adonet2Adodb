﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Martin.Dupuis.Adonet2Adodb;
using Martin.Dupuis.Adonet2Adodb.OutputFormats;
using Adonet2Adodb_tests.TestObjectsFactory;
using FluentAssertions;
using Xunit;

namespace Adonet2Adodb_tests.OfDataTable
{
    public class SingleColumnConversion
    {
        [Fact]
        public void Returns_converted_data_with_1_row()
        {
            // Arrange
            string expected = @"<xml xmlns:s='uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882'
	xmlns:dt='uuid:C2F41010-65B3-11d1-A29F-00AA00C14882'
	xmlns:rs='urn:schemas-microsoft-com:rowset'
	xmlns:z='#RowsetSchema'>
<s:Schema id='RowsetSchema'>
	<s:ElementType name='row' content='eltOnly'>
		<s:AttributeType name='EmployeeID' rs:number='1'>
			<s:datatype dt:type='string' dt:maxLength='255'/>
		</s:AttributeType>
		<s:extends type='rs:rowbase'/>
	</s:ElementType>
</s:Schema>
<rs:data>
	<z:row EmployeeID='1' />
</rs:data>
</xml>
";

            DataSet data = new CreateADataset()
                .WithATableNamed("Employee")
                .WithAFieldOfTypeString("EmployeeID")
                .BuildIt();
            DataRow dr = data.Tables["Employee"].NewRow();
            dr["EmployeeID"] = "1";
            data.Tables["Employee"].Rows.Add(dr);

            var converter = new DataTableConverter(data.Tables["Employee"]);

            // Act
            string result = converter.ToXMLRecordset();

            // Assert
            result.Should().Be(expected);
        }

        [Fact]
        public void Returns_converted_data_with_multiple_rows()
        {
            // Arrange
            string expected = @"<xml xmlns:s='uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882'
	xmlns:dt='uuid:C2F41010-65B3-11d1-A29F-00AA00C14882'
	xmlns:rs='urn:schemas-microsoft-com:rowset'
	xmlns:z='#RowsetSchema'>
<s:Schema id='RowsetSchema'>
	<s:ElementType name='row' content='eltOnly'>
		<s:AttributeType name='EmployeeID' rs:number='1'>
			<s:datatype dt:type='string' dt:maxLength='255'/>
		</s:AttributeType>
		<s:extends type='rs:rowbase'/>
	</s:ElementType>
</s:Schema>
<rs:data>
	<z:row EmployeeID='1' />
	<z:row EmployeeID='2' />
	<z:row EmployeeID='3' />
</rs:data>
</xml>
";

            DataSet data = new CreateADataset()
                .WithATableNamed("Employee")
                .WithAFieldOfTypeString("EmployeeID")
                .BuildIt();
            DataRow dr = data.Tables["Employee"].NewRow();
            dr["EmployeeID"] = "1";
            data.Tables["Employee"].Rows.Add(dr);

            dr = data.Tables["Employee"].NewRow();
            dr["EmployeeID"] = "2";
            data.Tables["Employee"].Rows.Add(dr);

            dr = data.Tables["Employee"].NewRow();
            dr["EmployeeID"] = "3";
            data.Tables["Employee"].Rows.Add(dr);

            var converter = new DataTableConverter(data.Tables["Employee"]);

            // Act
            string result = converter.ToXMLRecordset();

            // Assert
            result.Should().Be(expected);
        }

    }
}
