﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Martin.Dupuis.Adonet2Adodb;
using Martin.Dupuis.Adonet2Adodb.OutputFormats;
using Adonet2Adodb_tests.TestObjectsFactory;
using FluentAssertions;
using Xunit;

namespace Adonet2Adodb_tests.OfDataTable
{
    public class MultipleColumnsConversion
    {
        [Fact]
        public void Returns_converted_data()
        {
            // Arrange
            string expected = @"<xml xmlns:s='uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882'
	xmlns:dt='uuid:C2F41010-65B3-11d1-A29F-00AA00C14882'
	xmlns:rs='urn:schemas-microsoft-com:rowset'
	xmlns:z='#RowsetSchema'>
<s:Schema id='RowsetSchema'>
	<s:ElementType name='row' content='eltOnly'>
		<s:AttributeType name='UniqueID' rs:number='1'>
			<s:datatype dt:type='string' dt:maxLength='255'/>
		</s:AttributeType>
		<s:AttributeType name='Firstname' rs:number='2'>
			<s:datatype dt:type='string' dt:maxLength='255'/>
		</s:AttributeType>
		<s:AttributeType name='Lastname' rs:number='3'>
			<s:datatype dt:type='string' dt:maxLength='255'/>
		</s:AttributeType>
		<s:extends type='rs:rowbase'/>
	</s:ElementType>
</s:Schema>
<rs:data>
	<z:row UniqueID='1' Firstname='James' Lastname='Iha' />
</rs:data>
</xml>
";

            DataSet data = new CreateADataset()
                .WithATableNamed("BandMembers")
                .WithAFieldOfTypeString("UniqueID")
                .WithAFieldOfTypeString("Firstname")
                .WithAFieldOfTypeString("Lastname")
                .BuildIt();
            DataRow dr = data.Tables["BandMembers"].NewRow();
            dr["UniqueID"] = "1";
            dr["Firstname"] = "James";
            dr["Lastname"] = "Iha";
            data.Tables["BandMembers"].Rows.Add(dr);

            var converter = new DataTableConverter(data.Tables["BandMembers"]);

            // Act
            string result = converter.ToXMLRecordset();

            // Assert
            result.Should().Be(expected);
        }

        [Fact]
        public void Returns_converted_data_with_multiple_rows()
        {
            // Arrange
            string expected = @"<xml xmlns:s='uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882'
	xmlns:dt='uuid:C2F41010-65B3-11d1-A29F-00AA00C14882'
	xmlns:rs='urn:schemas-microsoft-com:rowset'
	xmlns:z='#RowsetSchema'>
<s:Schema id='RowsetSchema'>
	<s:ElementType name='row' content='eltOnly'>
		<s:AttributeType name='UniqueID' rs:number='1'>
			<s:datatype dt:type='string' dt:maxLength='255'/>
		</s:AttributeType>
		<s:AttributeType name='Firstname' rs:number='2'>
			<s:datatype dt:type='string' dt:maxLength='255'/>
		</s:AttributeType>
		<s:AttributeType name='Lastname' rs:number='3'>
			<s:datatype dt:type='string' dt:maxLength='255'/>
		</s:AttributeType>
		<s:extends type='rs:rowbase'/>
	</s:ElementType>
</s:Schema>
<rs:data>
	<z:row UniqueID='1' Firstname='James' Lastname='Iha' />
	<z:row UniqueID='2' Firstname='Billy' Lastname='Corgan' />
	<z:row UniqueID='3' Firstname='D'arcy' Lastname='Wretzky' />
</rs:data>
</xml>
";

            DataSet data = new CreateADataset()
                .WithATableNamed("BandMembers")
                .WithAFieldOfTypeString("UniqueID")
                .WithAFieldOfTypeString("Firstname")
                .WithAFieldOfTypeString("Lastname")
                .BuildIt();
            DataRow dr = data.Tables["BandMembers"].NewRow();
            dr["UniqueID"] = "1";
            dr["Firstname"] = "James";
            dr["Lastname"] = "Iha";
            data.Tables["BandMembers"].Rows.Add(dr);

            dr = data.Tables["BandMembers"].NewRow();
            dr["UniqueID"] = "2";
            dr["Firstname"] = "Billy";
            dr["Lastname"] = "Corgan";
            data.Tables["BandMembers"].Rows.Add(dr);

            dr = data.Tables["BandMembers"].NewRow();
            dr["UniqueID"] = "3";
            dr["Firstname"] = "D'arcy";
            dr["Lastname"] = "Wretzky";
            data.Tables["BandMembers"].Rows.Add(dr);

            var converter = new DataTableConverter(data.Tables["BandMembers"]);

            // Act
            string result = converter.ToXMLRecordset();

            // Assert
            result.Should().Be(expected);
        }
    }
}
