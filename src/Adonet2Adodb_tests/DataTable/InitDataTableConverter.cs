﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Martin.Dupuis.Adonet2Adodb;
using Martin.Dupuis.Adonet2Adodb.Exceptions;
using FluentAssertions;
using Xunit;

namespace Adonet2Adodb_tests.OfDataTable
{
    public class InitDataTableConverter
    {
        [Fact]
        public void Throw_ArgumentNullException_when_datatable_is_null()
        {
            // arrange
            DataTableConverter sut;

            // act
            Action act = () => sut = new DataTableConverter(null);

            // assert
            act.Should().Throw<ArgumentNullException>();
        }

        [Fact]
        public void Throw_NoFieldInDataTableException_when_datatable_is_null()
        {
            // arrange
            DataTable dt = new DataTable();
            DataTableConverter sut;

            // act
            Action act = () => sut = new DataTableConverter(dt);

            // assert
            act.Should().Throw<NoFieldInDataTableException>();
        }

        [Fact]
        public void Does_not_throw_NoFieldInDataTableException_when_DataTable_has_one_Field()
        {
            // arrange
            DataTable dt = new DataTable("Table1");
            dt.Columns.Add(new DataColumn("Col1"));
            DataTableConverter sut;

            // act
            Action act = () => sut = new DataTableConverter(dt);

            // assert
            act.Should().NotThrow<NoFieldInDataTableException>();
        }
    }
}
