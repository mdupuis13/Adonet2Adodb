using Adonet2Adodb_tests.TestObjectsFactory;
using System.Data;
using FluentAssertions;
using Xunit;
using Martin.Dupuis.Adonet2Adodb;

namespace Adonet2Adodb_tests.OfDataSet
{
    public class TwoTablesDataset2Xml
    {
        private const string _expected = @"<xml xmlns:s='uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882'
	xmlns:dt='uuid:C2F41010-65B3-11d1-A29F-00AA00C14882'
	xmlns:rs='urn:schemas-microsoft-com:rowset'
	xmlns:z='#RowsetSchema'>
<s:Schema id='RowsetSchema'>
	<s:ElementType name='row' content='eltOnly'>
		<s:AttributeType name='UniqueID' rs:number='1'>
			<s:datatype dt:type='string' dt:maxLength='255'/>
		</s:AttributeType>
		<s:AttributeType name='Firstname' rs:number='2'>
			<s:datatype dt:type='string' dt:maxLength='255'/>
		</s:AttributeType>
		<s:AttributeType name='Lastname' rs:number='3'>
			<s:datatype dt:type='string' dt:maxLength='255'/>
		</s:AttributeType>
		<s:extends type='rs:rowbase'/>
	</s:ElementType>
</s:Schema>
<rs:data>
	<z:row UniqueID='100' Firstname='Billy' Lastname='Corgan' />
	<z:row UniqueID='200' Firstname='James' Lastname='Iha' />
	<z:row UniqueID='300' Firstname='D'arcy' Lastname='Wretzky' />
</rs:data>
</xml>
<xml xmlns:s='uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882'
	xmlns:dt='uuid:C2F41010-65B3-11d1-A29F-00AA00C14882'
	xmlns:rs='urn:schemas-microsoft-com:rowset'
	xmlns:z='#RowsetSchema'>
<s:Schema id='RowsetSchema'>
	<s:ElementType name='row' content='eltOnly'>
		<s:AttributeType name='refUniqueID' rs:number='1'>
			<s:datatype dt:type='string' dt:maxLength='255'/>
		</s:AttributeType>
		<s:AttributeType name='Street_Address' rs:number='2'>
			<s:datatype dt:type='string' dt:maxLength='255'/>
		</s:AttributeType>
		<s:AttributeType name='City' rs:number='3'>
			<s:datatype dt:type='string' dt:maxLength='255'/>
		</s:AttributeType>
		<s:AttributeType name='PostCode' rs:number='4'>
			<s:datatype dt:type='string' dt:maxLength='255'/>
		</s:AttributeType>
		<s:extends type='rs:rowbase'/>
	</s:ElementType>
</s:Schema>
<rs:data>
	<z:row refUniqueID='100' Street_Address='123 smashing st.' City='Seattle' PostCode='90210' />
	<z:row refUniqueID='200' Street_Address='456 pumpkin ave.' City='Chicago' PostCode='43567' />
	<z:row refUniqueID='300' Street_Address='7890 heartly road' City='Miami' PostCode='76543' />
</rs:data>
</xml>
";

        private DataSet GetDefaultTestDataset()
        {
            DataSet ds = new CreateADataset()
                .WithATableNamed("BandMembers")
                    .WithAFieldOfTypeString("UniqueID")
                    .WithAFieldOfTypeString("Firstname")
                    .WithAFieldOfTypeString("Lastname")
                .WithATableNamed("Address")
                    .WithAFieldOfTypeString("refUniqueID")
                    .WithAFieldOfTypeString("Street_Address")
                    .WithAFieldOfTypeString("City")
                    .WithAFieldOfTypeString("PostCode")
                .BuildIt();

            ds.Tables["BandMembers"].Rows.Add(CreatEmployeeRow(ds.Tables["BandMembers"].NewRow(), "100", "Billy", "Corgan"));
            ds.Tables["BandMembers"].Rows.Add(CreatEmployeeRow(ds.Tables["BandMembers"].NewRow(), "200", "James", "Iha"));
            ds.Tables["BandMembers"].Rows.Add(CreatEmployeeRow(ds.Tables["BandMembers"].NewRow(), "300", "D'arcy", "Wretzky"));

            ds.Tables["Address"].Rows.Add(CreatAddressRow(ds.Tables["Address"].NewRow(), "100", "123 smashing st.", "Seattle", "90210"));
            ds.Tables["Address"].Rows.Add(CreatAddressRow(ds.Tables["Address"].NewRow(), "200", "456 pumpkin ave.", "Chicago", "43567"));
            ds.Tables["Address"].Rows.Add(CreatAddressRow(ds.Tables["Address"].NewRow(), "300", "7890 heartly road", "Miami", "76543"));
            return ds;
        }

        private DataRow CreatEmployeeRow(DataRow dr, string UniqueID, string Firstname, string Lastname)
        {
            dr["UniqueID"] = UniqueID;
            dr["Firstname"] = Firstname;
            dr["Lastname"] = Lastname;
            return dr;
        }
        private DataRow CreatAddressRow(DataRow dr, string refUniqueID, string Street_Address, string City, string PostCode)
        {
            dr["refUniqueID"] = refUniqueID;
            dr["Street_Address"] = Street_Address;
            dr["City"] = City;
            dr["PostCode"] = PostCode;
            return dr;
        }

        [Fact]
        public void Happy_path_ends_with_success()
        {
            // arrange
            DataSetConverter converter = new DataSetConverter(GetDefaultTestDataset());

            // act
            string result = converter.ToXMLRecordset();

            // assert
            result.Should().Be(_expected);
        }
    }
}
