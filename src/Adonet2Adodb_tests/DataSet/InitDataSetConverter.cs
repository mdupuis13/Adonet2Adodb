using Martin.Dupuis.Adonet2Adodb;
using System;
using System.Data;
using FluentAssertions;
using Xunit;
using Martin.Dupuis.Adonet2Adodb.Exceptions;

namespace Adonet2Adodb_tests.OfDataSet
{
    public class InitDataSetConverter
    {
        [Fact]
        public void Throw_ArgumentNullException_when_dataset_is_null()
        {
            // arrange
            DataSetConverter sut;

            // act
            Action act = () => sut = new DataSetConverter(null);

            // assert
            act.Should().Throw<ArgumentNullException>();
        }
        
        [Fact]
        public void Throw_NoTablesInDatasetException_when_Dataset_has_no_tables()
        {
            // arrange
            DataSet ds = new DataSet();
            DataSetConverter sut;

            // act
            Action act = () => sut = new DataSetConverter(ds);

            // assert
            act.Should().Throw<NoTablesInDatasetException>();
        }

        [Fact]
        public void Does_not_throw_NoTablesInDatasetException_when_Dataset_has_one_table()
        {
            // arrange
            DataSet ds = new DataSet();
            ds.Tables.Add(new DataTable("Table1"));
            DataSetConverter sut;

            // act
            Action act = () => sut = new DataSetConverter(ds);

            // assert
            act.Should().NotThrow<NoTablesInDatasetException>();
        }
    }
}
