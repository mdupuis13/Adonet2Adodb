using Martin.Dupuis.Adonet2Adodb;
using Adonet2Adodb_tests.TestObjectsFactory;
using System.Data;
using FluentAssertions;
using Xunit;

namespace Adonet2Adodb_tests.OfDataSet
{
    public class SingleTableDataset2Xml
    {
        private const string _expected = @"<xml xmlns:s='uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882'
	xmlns:dt='uuid:C2F41010-65B3-11d1-A29F-00AA00C14882'
	xmlns:rs='urn:schemas-microsoft-com:rowset'
	xmlns:z='#RowsetSchema'>
<s:Schema id='RowsetSchema'>
	<s:ElementType name='row' content='eltOnly'>
		<s:AttributeType name='UniqueID' rs:number='1'>
			<s:datatype dt:type='string' dt:maxLength='255'/>
		</s:AttributeType>
		<s:AttributeType name='Firstname' rs:number='2'>
			<s:datatype dt:type='string' dt:maxLength='255'/>
		</s:AttributeType>
		<s:AttributeType name='Lastname' rs:number='3'>
			<s:datatype dt:type='string' dt:maxLength='255'/>
		</s:AttributeType>
		<s:extends type='rs:rowbase'/>
	</s:ElementType>
</s:Schema>
<rs:data>
	<z:row UniqueID='100' Firstname='Billy' Lastname='Corgan' />
	<z:row UniqueID='200' Firstname='James' Lastname='Iha' />
	<z:row UniqueID='300' Firstname='D'arcy' Lastname='Wretzky' />
</rs:data>
</xml>
";

        private DataSet GetDefaultTestDataset()
        {
            DataSet ds = new CreateADataset()
                .WithATableNamed("BandMembers")
                    .WithAFieldOfTypeString("UniqueID")
                    .WithAFieldOfTypeString("Firstname")
                    .WithAFieldOfTypeString("Lastname")
                .BuildIt();

            ds.Tables["BandMembers"].Rows.Add(CreateRow(ds, "100", "Billy", "Corgan"));
            ds.Tables["BandMembers"].Rows.Add(CreateRow(ds, "200", "James", "Iha"));
            ds.Tables["BandMembers"].Rows.Add(CreateRow(ds, "300", "D'arcy", "Wretzky"));

            return ds;
        }

        private DataRow CreateRow(DataSet ds, string UniqueID, string Firstname, string Lastname)
        {
            DataRow dr = ds.Tables[0].NewRow();
            dr["UniqueID"] = UniqueID;
            dr["Firstname"] = Firstname;
            dr["Lastname"] = Lastname;
            return dr;
        }

        [Fact]
        public void Happy_path_ends_with_success()
        {
            // arrange
            DataSetConverter converter = new DataSetConverter(GetDefaultTestDataset());

            // act
            string result = converter.ToXMLRecordset();

            // assert
            result.Should().Be(_expected);
        }
    }
}
