﻿using System;

namespace Adonet2Adodb_tests.TestObjectsFactory
{
    public class TableHasNoFieldException : Exception
    {
        public TableHasNoFieldException(string Message) : base(Message)
        {
            
        }
    }
}
