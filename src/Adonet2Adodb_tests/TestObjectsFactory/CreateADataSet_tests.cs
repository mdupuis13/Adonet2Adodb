﻿using System;
using System.Data;
using FluentAssertions;
using Xunit;

namespace Adonet2Adodb_tests.TestObjectsFactory
{
    public class CreateADataSet_tests
    {
        public class GivenANewDataset
        {
            public class When_it_is_created_with_CreateADataset
            {
                [Fact]
                public void Then_it_should_throw_Exception_if_no_table_was_added()
                {
                    // arrange
                    DataSet sut = new DataSet();

                    // act
                    Action act = () => sut = new CreateADataset().BuildIt();

                    // assert
                    act.Should().Throw<InvalidOperationException>()
                        .WithMessage("A table must be added first.");

                    sut.Tables.Count.Should().Be(0);
                }

                [Fact]
                public void Then_it_should_have_at_least_one_table()
                {
                    // arrange
                    DataSet sut = new DataSet();

                    // act
                    Action act = () => sut = new CreateADataset()
                        .WithATableNamed("Table1")
                        .BuildIt();

                    // assert
                    act.Should().NotThrow<InvalidOperationException>();

                    sut.Tables.Count.Should().Be(1);
                }

            }

            public class When_a_table_is_added
            {
                [Fact]
                public void Then_it_should_throw_ArgumentNullException_if_empty_name_is_given_for_Tablename()
                {
                    // arrange
                    DataSet sut = new DataSet();

                    // act
                    Action act = () => sut = new CreateADataset()
                        .WithATableNamed("")
                        .BuildIt();

                    // assert
                    act.Should().Throw<ArgumentNullException>();
                }

                [Fact]
                public void Then_it_should_throw_ArgumentNullException_if_null_is_given_for_Tablename()
                {
                    // arrange
                    DataSet sut = new DataSet();

                    // act
                    Action act = () => sut = new CreateADataset()
                        .WithATableNamed(null)
                        .BuildIt();

                    // assert
                    act.Should().Throw<ArgumentNullException>();
                }

                [Fact]
                public void Then_it_must_have_a_name()
                {
                    // arrange
                    const string TableName = "Table1";
                    DataSet sut = new DataSet();

                    // act
                    sut = new CreateADataset()
                        .WithATableNamed(TableName)
                        .BuildIt();

                    // assert
                    sut.Tables[0].TableName.Should().Be(TableName);
                }
            }

            public class When_a_string_field_is_added_to_a_table
            {
                [Fact]
                public void Then_it_should_throw_FieldAddedToNullTableException_if_no_table_exists()
                {
                    // arrange
                    DataSet sut = new DataSet();

                    // act
                    Action act = () => sut = new CreateADataset()
                        .WithAFieldOfTypeString("Field1")
                        .BuildIt();

                    // assert
                    act.Should().Throw<FieldAddedToNullTableException>();
                }

                [Fact]
                public void Then_it_should_throw_ArgumentNullException_if_empty_name_is_given_for_Fieldname()
                {
                    // arrange
                    DataSet sut = new DataSet();

                    // act
                    Action act = () => sut = new CreateADataset()
                        .WithATableNamed("Table1")
                        .WithAFieldOfTypeString("")
                        .BuildIt();

                    // assert
                    act.Should().Throw<ArgumentNullException>();
                }

                [Fact]
                public void Then_it_should_throw_ArgumentNullException_if_null_is_given_for_Fieldname()
                {
                    // arrange
                    DataSet sut = new DataSet();

                    // act
                    Action act = () => sut = new CreateADataset()
                        .WithATableNamed("Table1")
                        .WithAFieldOfTypeString(null)
                        .BuildIt();

                    // assert
                    act.Should().Throw<ArgumentNullException>();
                }

                [Fact]
                public void Then_it_is_added_to_a_table()
                {
                    // arrange
                    const string FieldName = "Field1";
                    DataSet sut = new DataSet();

                    // act
                    sut = new CreateADataset()
                        .WithATableNamed("Table1")
                        .WithAFieldOfTypeString(FieldName)
                        .BuildIt();

                    // assert
                    sut.Tables[0].Columns.Count.Should().Be(1);
                    sut.Tables[0].Columns[0].ColumnName.Should().Be(FieldName);
                }

                [Fact]
                public void Then_it_must_have_a_name()
                {
                    // arrange
                    const string FieldName = "Field1";
                    DataSet sut = new DataSet();

                    // act
                    sut = new CreateADataset()
                        .WithATableNamed("Table1")
                        .WithAFieldOfTypeString(FieldName)
                        .BuildIt();

                    // assert
                    sut.Tables[0].Columns.Count.Should().Be(1);
                    sut.Tables[0].Columns[0].ColumnName.Should().Be(FieldName);
                }
            }

            public class When_two_string_fields_are_added_to_a_table
            {
                [Fact]
                public void Then_they_should_both_be_present()
                {
                    // arrange
                    const string Field1Name = "Field1";
                    const string Field2Name = "Field2";
                    DataSet sut = new DataSet();

                    // act
                    sut = new CreateADataset()
                        .WithATableNamed("Table1")
                        .WithAFieldOfTypeString(Field1Name)
                        .WithAFieldOfTypeString(Field2Name)
                        .BuildIt();

                    // assert
                    sut.Tables[0].Columns.Count.Should().Be(2);
                    sut.Tables[0].Columns[0].ColumnName.Should().Be(Field1Name);
                    sut.Tables[0].Columns[1].ColumnName.Should().Be(Field2Name);

                }
            }

            public class When_a_string_field_is_added_to_the_second_table
            {
                [Fact]
                public void Then_it_is_added_to_the_second_table()
                {
                    // arrange
                    DataSet sut = new DataSet();

                    // act
                    sut = new CreateADataset()
                        .WithATableNamed("Table1")
                        .WithAFieldOfTypeString("Field1")
                        .WithATableNamed("Employee")
                        .WithAFieldOfTypeString("EmployeeID")
                        .BuildIt();

                    // assert
                    sut.Tables.Count.Should().Be(2);
                    sut.Tables[0].TableName.Should().Be("Table1");
                    sut.Tables[0].Columns.Count.Should().Be(1);
                    sut.Tables[0].Columns[0].ColumnName.Should().Be("Field1");

                    sut.Tables[1].TableName.Should().Be("Employee");
                    sut.Tables[1].Columns.Count.Should().Be(1);
                    sut.Tables[1].Columns[0].ColumnName.Should().Be("EmployeeID");
                }

            }
        }
    }
}
