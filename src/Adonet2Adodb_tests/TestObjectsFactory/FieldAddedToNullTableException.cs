﻿using System;

namespace Adonet2Adodb_tests.TestObjectsFactory
{
    public class FieldAddedToNullTableException : Exception
    {
        public FieldAddedToNullTableException(string Message) : base(Message)
        {

        }
    }
}
