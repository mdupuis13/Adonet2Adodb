﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Adonet2Adodb_tests.TestObjectsFactory
{
    /// <summary>
    /// Creates a new dataset from scratch.
    /// </summary>
    public class CreateADataset
    {
        private readonly DataSet _dataset;
        private bool _tableHasField = false;
        private bool _newTableAdded = false;

        /// <summary>
        /// Creates a new dataset from scratch.
        /// </summary>
        public CreateADataset()
        {
            _dataset = new DataSet();
        }

        /// <summary>
        /// Add a table to the dataset
        /// </summary>
        /// <param name="Tablename">Name of the table to be added</param>
        /// <returns>CreateADataset object with an additionnal table named according to Tablename</returns>
        public CreateADataset WithATableNamed(string Tablename)
        {
            if ( string.IsNullOrEmpty(Tablename) )
                throw new ArgumentNullException(nameof(Tablename), $"The argument {nameof(Tablename)} cannot be null.");

            if (_newTableAdded & !_tableHasField )
                throw new TableHasNoFieldException($"You cannot call {nameof(WithATableNamed)} twice. A field must be added tothe first table.");

            DataTable newTable = new DataTable(Tablename);
            _dataset.Tables.Add(newTable);

            _newTableAdded = true;
            _tableHasField = false;
            
            return this;
        }

        /// <summary>
        /// Add a field to the last table added with <see cref="WithATableNamed(string)"/>
        /// </summary>
        /// <param name="Fieldname">Name of the field to be added</param>
        /// <returns>CreateADataset object with an additionnal field (column) named according to Fieldname in the lastly added table</returns>
        public CreateADataset WithAFieldOfTypeString(string Fieldname)
        {
            if ( string.IsNullOrEmpty(Fieldname) )
                throw new ArgumentNullException(nameof(Fieldname), $"The argument {nameof(Fieldname)} cannot be null.");

            if ( _dataset.Tables.Count == 0)
                throw new FieldAddedToNullTableException($"A table must be added first.");
            
            DataColumn newColumn = new DataColumn(Fieldname, typeof(string));

            _dataset.Tables[_dataset.Tables.Count - 1].Columns.Add(newColumn);

            _newTableAdded = false;
            _tableHasField = true;
            return this;
        }

        /// <summary>
        /// Return the completed object
        /// </summary>
        /// <returns>CreateADataset object</returns>
        public DataSet BuildIt()
        {
            if (_dataset.Tables.Count == 0)
                throw new InvalidOperationException($"A table must be added first.");
            return _dataset;
        }
    }
}
