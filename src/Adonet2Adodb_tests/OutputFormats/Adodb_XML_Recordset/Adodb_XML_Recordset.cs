﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Martin.Dupuis.Adonet2Adodb.OutputFormats;
using Adonet2Adodb_tests.TestObjectsFactory;
using FluentAssertions;
using Xunit;

namespace Adonet2Adodb_tests.OfOutputFormats
{
    public class Adodb_XML_Recordset
    {
        [Fact]
        public void BuildXML_returns_properly_formatted_header()
        {
            // arrange
            string expected = "<xml xmlns:s='uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882'\r\n\t" +
                              "xmlns:dt='uuid:C2F41010-65B3-11d1-A29F-00AA00C14882'\r\n\t" +
                              "xmlns:rs='urn:schemas-microsoft-com:rowset'\r\n\t" +
                              "xmlns:z='#RowsetSchema'>\r\n";

            Martin.Dupuis.Adonet2Adodb.OutputFormats.Adodb_XML_Recordset sut = new Martin.Dupuis.Adonet2Adodb.OutputFormats.Adodb_XML_Recordset();

            // act
            string result = sut.BuildXML();

            // assert
            result.Should().Contain(expected);
        }

        [Fact]
        public void BuildXML_returns_properly_formatted_rowset_header()
        {
            // arrange
            string expected = "<s:Schema id='RowsetSchema'>\r\n\t" +
                              "<s:ElementType name='row' content='eltOnly'>\r\n";

            Martin.Dupuis.Adonet2Adodb.OutputFormats.Adodb_XML_Recordset sut = new Martin.Dupuis.Adonet2Adodb.OutputFormats.Adodb_XML_Recordset();

            // act
            string result = sut.BuildXML();

            // assert
            result.Should().Contain(expected);
        }

        [Fact]
        public void BuildXML_WithColumn_adds_a_column_definition()
        {
            // arrange
            const string colname = "EmployeeID";
            string expected = $"\t\t<s:AttributeType name='{colname}' rs:number='1'>\r\n";

            DataSet ds = new CreateADataset()
                .WithATableNamed("Employee")
                .WithAFieldOfTypeString(colname)
                .BuildIt();
            Martin.Dupuis.Adonet2Adodb.OutputFormats.Adodb_XML_Recordset sut = new Martin.Dupuis.Adonet2Adodb.OutputFormats.Adodb_XML_Recordset();
            sut.WithColumns(ds.Tables[0].Columns);

            // act
            string result = sut.BuildXML();

            // assert
            result.Should().Contain(expected);
        }

        [Fact]
        public void BuildXML_WithColumn_twice_adds_two_column_definitions()
        {
            // arrange
            const string col1name = "Firstname";
            const string col2name = "Lastname";
            string expected1 = $"\t\t<s:AttributeType name='{col1name}'";
            string expected2 = $"\t\t<s:AttributeType name='{col2name}'";

            DataSet ds = new CreateADataset()
                .WithATableNamed("Employee")
                .WithAFieldOfTypeString(col1name)
                .WithAFieldOfTypeString(col2name)
                .BuildIt();
            Martin.Dupuis.Adonet2Adodb.OutputFormats.Adodb_XML_Recordset sut = new Martin.Dupuis.Adonet2Adodb.OutputFormats.Adodb_XML_Recordset();
            sut.WithColumns(ds.Tables[0].Columns);

            // act
            string result = sut.BuildXML();

            // assert
            result.Should().Contain(expected1);
            result.Should().Contain(expected2);
        }

        [Fact]
        public void BuildXML_WithColumn_twice_preserves_order_of_columns()
        {
            // arrange
            const string col1name = "Firstname";
            const string col2name = "Lastname";
            string expected1 = $"\t\t<s:AttributeType name='{col1name}' rs:number='1'>\r\n";
            string expected2 = $"\t\t<s:AttributeType name='{col2name}' rs:number='2'>\r\n";

            DataSet ds = new CreateADataset()
                .WithATableNamed("Employee")
                .WithAFieldOfTypeString(col1name)
                .WithAFieldOfTypeString(col2name)
                .BuildIt();
            Martin.Dupuis.Adonet2Adodb.OutputFormats.Adodb_XML_Recordset sut = new Martin.Dupuis.Adonet2Adodb.OutputFormats.Adodb_XML_Recordset();
            sut.WithColumns(ds.Tables[0].Columns);

            // act
            string result = sut.BuildXML();

            // assert
            result.Should().Contain(expected1);
            result.Should().Contain(expected2);
        }

        [Fact]
        public void BuildXML_adds_schema_definition_footer()
        {
            // arrange
            string expected = "\t\t<s:extends type='rs:rowbase'/>\r\n" +
                              "\t</s:ElementType>\r\n" + 
                              "</s:Schema>\r\n";
            
            DataSet ds = new CreateADataset()
                .WithATableNamed("Employee")
                .WithAFieldOfTypeString("EmployeeID")
                .BuildIt();
            Martin.Dupuis.Adonet2Adodb.OutputFormats.Adodb_XML_Recordset sut = new Martin.Dupuis.Adonet2Adodb.OutputFormats.Adodb_XML_Recordset();
            sut.WithColumns(ds.Tables[0].Columns);

            // act
            string result = sut.BuildXML();

            // assert
            result.Should().Contain(expected);
        }

        [Fact]
        public void BuildXML_returns_rows_of_DataTable()
        {
            // arrange
            const string col1name = "Firstname";
            const string col2name = "Lastname";

            string expected = @"<z:row Firstname='Billy' Lastname='Corgan' />";

            DataSet ds = new CreateADataset()
                            .WithATableNamed("Employee")
                            .WithAFieldOfTypeString(col1name)
                            .WithAFieldOfTypeString(col2name)
                            .BuildIt();
            DataRow dr = ds.Tables["Employee"].NewRow();
            dr[col1name] = "Billy";
            dr[col2name] = "Corgan";
            ds.Tables["Employee"].Rows.Add(dr);

            Martin.Dupuis.Adonet2Adodb.OutputFormats.Adodb_XML_Recordset sut = new Martin.Dupuis.Adonet2Adodb.OutputFormats.Adodb_XML_Recordset();
            sut.WithColumns(ds.Tables["Employee"].Columns)
               .WithData(ds.Tables["Employee"].Rows);

            // act
            string result = sut.BuildXML();

            // assert
            result.Should().Contain(expected);
        }

        [Fact]
        public void BuildXML_returns_properly_formatted_data_footer()
        {
            // arrange
            const string col1name = "Firstname";
            const string col2name = "Lastname";

            string expected = "</rs:data>";

            DataSet ds = new CreateADataset()
                            .WithATableNamed("Employee")
                            .WithAFieldOfTypeString(col1name)
                            .WithAFieldOfTypeString(col2name)
                            .BuildIt();
            DataRow dr = ds.Tables["Employee"].NewRow();
            dr[col1name] = "Billy";
            dr[col2name] = "Corgan";
            ds.Tables["Employee"].Rows.Add(dr);

            Martin.Dupuis.Adonet2Adodb.OutputFormats.Adodb_XML_Recordset sut = new Martin.Dupuis.Adonet2Adodb.OutputFormats.Adodb_XML_Recordset();
            sut.WithColumns(ds.Tables["Employee"].Columns)
               .WithData(ds.Tables["Employee"].Rows);

            // act
            string result = sut.BuildXML();

            // assert
            result.Should().Contain(expected);
        }

        [Fact]
        public void BuildXML_returns_properly_formatted_footer()
        {
            // arrange
            string expected = "</xml>";

            Martin.Dupuis.Adonet2Adodb.OutputFormats.Adodb_XML_Recordset sut = new Martin.Dupuis.Adonet2Adodb.OutputFormats.Adodb_XML_Recordset();

            // act
            string result = sut.BuildXML();

            // assert
            result.Should().Contain(expected);
        }
    }
}
