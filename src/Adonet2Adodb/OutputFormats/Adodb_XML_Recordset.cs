﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Runtime.CompilerServices;
using System.Text;

[assembly: InternalsVisibleToAttribute("Adonet2Adodb_tests")]
namespace Martin.Dupuis.Adonet2Adodb.OutputFormats
{
    internal class Adodb_XML_Recordset
    {
        private const string NewLine = "\r\n";
    
        List<DataColumn> _columns = new List<DataColumn>();
        DataRowCollection _rows;


        public string BuildXML()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(GetXmlHeader());
            sb.Append(GetRowsetHeader());
            sb.Append(GetColumnsDefinition());
            sb.Append(GetRowsetFooter());
            sb.Append(GetDataRows());
            sb.Append(GetXmlFooter());

            return sb.ToString();
        }

        public Adodb_XML_Recordset WithColumns(DataColumnCollection columns)
        {
            foreach (DataColumn column in columns)
                _columns.Add(column);

            return this;
        }

        public Adodb_XML_Recordset WithData(DataRowCollection rows)
        {
            _rows = rows;

            return this;
        }

        private string GetXmlHeader()
        {
            StringBuilder rootElement = new StringBuilder();
            rootElement.Append("<xml xmlns:s=");
            rootElement.Append($"'uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882'{NewLine}\t");
            rootElement.Append("xmlns:dt=");
            rootElement.Append($"'uuid:C2F41010-65B3-11d1-A29F-00AA00C14882'{NewLine}\t");
            rootElement.Append($"xmlns:rs='urn:schemas-microsoft-com:rowset'{NewLine}\t");
            rootElement.Append($"xmlns:z='#RowsetSchema'>{NewLine}");

            return rootElement.ToString();
        }

        private string GetRowsetHeader()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append($"<s:Schema id='RowsetSchema'>{NewLine}\t");
            sb.Append($"<s:ElementType name='row' content='eltOnly'>{NewLine}");
            
            return sb.ToString();
        }

        private string GetColumnsDefinition()
        {
            int index = 0;

            StringBuilder sb = new StringBuilder();

            foreach (DataColumn dc in _columns)
            {
                index++;
                sb.Append($"\t\t<s:AttributeType name='{dc.ColumnName}' rs:number='{index}'>{NewLine}");
                sb.Append($"\t\t\t<s:datatype dt:type='string' dt:maxLength='255'/>{NewLine}");
                sb.Append($"\t\t</s:AttributeType>{NewLine}");
            }

            return sb.ToString();
        }

        private string GetRowsetFooter()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append($"\t\t<s:extends type='rs:rowbase'/>{NewLine}");
            sb.Append($"\t</s:ElementType>{NewLine}");
            sb.Append($"</s:Schema>{NewLine}");

            return sb.ToString();
        }

        private string GetDataRows()
        {
            int index = 0;
            StringBuilder sb = new StringBuilder();

            if (_rows != null)
            {
                sb.Append($"<rs:data>{NewLine}");

                foreach (DataRow dr in _rows)
                {
                    index++;
                    sb.Append($"\t<z:row ");

                    foreach (DataColumn dc in _columns)
                    {
                        sb.Append($"{dc.ColumnName}='{dr[dc.ColumnName].ToString()}' ");
                    }

                    sb.Append($"/>{NewLine}");
                }

                sb.Append($"</rs:data>{NewLine}");
            }

            return sb.ToString();
        }

        private string GetXmlFooter()
        {
            return $"</xml>{NewLine}";
        }
    }
}
