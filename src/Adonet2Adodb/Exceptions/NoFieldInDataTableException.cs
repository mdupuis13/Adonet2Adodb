﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Martin.Dupuis.Adonet2Adodb.Exceptions
{
    public class NoFieldInDataTableException : Exception
    {
        public NoFieldInDataTableException() : base("The DataTable received contains no Columns. At least one DataColumn must be present into the 'Columns' collection.")
        { }
    }
}
