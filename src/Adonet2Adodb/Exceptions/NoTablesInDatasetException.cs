﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Martin.Dupuis.Adonet2Adodb.Exceptions
{
    public class NoTablesInDatasetException : Exception
    {
        public NoTablesInDatasetException() :  base("The Dataset received contains no DataTable. At least one DataTable must be present into the 'Tables' collection.")
        { }
    }
}
