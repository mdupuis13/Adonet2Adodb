﻿using Martin.Dupuis.Adonet2Adodb.Exceptions;
using Martin.Dupuis.Adonet2Adodb.Interfaces;
using Martin.Dupuis.Adonet2Adodb.OutputFormats;
using System;
using System.Data;
using System.Text;

namespace Martin.Dupuis.Adonet2Adodb
{
    public class DataTableConverter : IConvert2XML
    {
        private DataTable _datatableToConvert;

        public DataTableConverter(DataTable datatable)
        {            
            _datatableToConvert = datatable ?? throw new ArgumentNullException(nameof(datatable));

            if (_datatableToConvert.Columns.Count == 0)
                throw new NoFieldInDataTableException();
        }

        public string ToXMLRecordset()
        {
            Adodb_XML_Recordset converter = new Adodb_XML_Recordset();

            converter
                .WithColumns(_datatableToConvert.Columns)
                .WithData(_datatableToConvert.Rows);

            return converter.BuildXML();
        }
    }
}
