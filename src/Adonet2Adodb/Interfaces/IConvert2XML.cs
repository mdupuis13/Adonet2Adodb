﻿using System.Data;

namespace Martin.Dupuis.Adonet2Adodb.Interfaces
{
    public interface IConvert2XML
    {
        string ToXMLRecordset();
    }
}
