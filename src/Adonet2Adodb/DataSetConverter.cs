﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Martin.Dupuis.Adonet2Adodb.Exceptions;
using Martin.Dupuis.Adonet2Adodb.Interfaces;

namespace Martin.Dupuis.Adonet2Adodb
{
    public class DataSetConverter : IConvert2XML
    {
        private DataSet _datasetToConvert;

        public DataSetConverter(DataSet dataset)
        {
            _datasetToConvert = dataset ?? throw new ArgumentNullException(nameof(dataset));

            if (_datasetToConvert.Tables.Count == 0)
                throw new NoTablesInDatasetException();
        }

        public string ToXMLRecordset()
        {
            StringBuilder sb = new StringBuilder();

            foreach (DataTable tbl in _datasetToConvert.Tables)
            {
                sb.Append(new DataTableConverter(tbl).ToXMLRecordset());
            }

            return sb.ToString();
        }
    }
}
