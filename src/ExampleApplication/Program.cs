﻿using System;

namespace ExampleApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            ConvertDatasetForAdodb convert = new ConvertDatasetForAdodb();

            Console.WriteLine("Dataset with one table.");
            Console.WriteLine(convert.Return_dataset_one_table());

            Console.WriteLine("Press <ENTER> to continue...");
            Console.ReadLine();

            Console.WriteLine("Dataset with two tables.");
            Console.WriteLine(convert.Return_dataset_two_table());

            Console.WriteLine("Press <ENTER> to end program...");
            Console.ReadLine();
        }
    }
}
