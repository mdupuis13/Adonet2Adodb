﻿using System;
using System.Collections.Generic;
using System.Text;
using Martin.Dupuis.Adonet2Adodb.Interfaces;
using Martin.Dupuis.Adonet2Adodb;
using System.Data;

namespace ExampleApplication
{
    internal class ConvertDatasetForAdodb
    {
        public string Return_dataset_one_table()
        {
            IConvert2XML converter = new DataSetConverter(ReadOneTableExampleData());
            return converter.ToXMLRecordset();
        }

        public string Return_dataset_two_table()
        {
            IConvert2XML converter = new DataSetConverter(ReadTwoTablesExampleData());
            return converter.ToXMLRecordset();
        }

        private DataSet ReadOneTableExampleData()
        {
            DataSet ds = new DataSet();
            ds.Tables.Add(GetEmployees());

            return ds;
        }

        private DataSet ReadTwoTablesExampleData()
        {
            DataSet ds = new DataSet();
            ds.Tables.Add(GetEmployees());
            ds.Tables.Add(GetOrders());

            return ds;
        }

        private DataTable GetEmployees()
        {
            DataTable dt = new DataTable("Employees");
            dt.Columns.Add(new DataColumn("EmployeeID", typeof(string)));
            dt.Columns.Add(new DataColumn("EmployeeName", typeof(string)));
            dt.Columns.Add(new DataColumn("Salary", typeof(string)));

            DataRow dr = dt.NewRow();
            dr["EmployeeID"] = "1234";
            dr["EmployeeName"] = "Marty McFly";
            dr["Salary"] = "12345$";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["EmployeeID"] = "1234";
            dr["EmployeeName"] = "George McFly";
            dr["Salary"] = "45678$";
            dt.Rows.Add(dr);

            return dt;
        }

        private DataTable GetOrders()
        {
            DataTable dt = new DataTable("Orders");
            dt.Columns.Add(new DataColumn("OrderID", typeof(string)));
            dt.Columns.Add(new DataColumn("CustomerName", typeof(string)));
            dt.Columns.Add(new DataColumn("OrderTotal", typeof(string)));

            DataRow dr = dt.NewRow();
            dr["OrderID"] = "1";
            dr["CustomerName"] = "Total Corp.";
            dr["OrderTotal"] = "654321$";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["OrderID"] = "2";
            dr["CustomerName"] = "Gazprom";
            dr["OrderTotal"] = "980123$";
            dt.Rows.Add(dr);

            return dt;
        }
    }
}
