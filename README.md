# Adonet2Adodb

## Overview

`Adonet2Adodb` is a library that converts an Ado.Net DataTable (or a DataSet.Tables collection) into the [ADO XML Persistence Format][MS-PRSTFR].

When converting a DataSet with multiple DataTables, the result of each DataTables' conversion is concatenated into a single string. It is left to the implementer to divide the different Recordsets.

## Install

It's available via [a nuget package](https://www.nuget.org/packages/Adonet2Adodb/)  
    
    PM> Install-Package Adonet2Adodb

## Example Usage
### DataSet
```csharp
var myDataSet as new DataSet()
// ... fill dataset with data here

var converter = new DataSetConverter(myDataSet);

string result = converter.ToXMLRecordset();

Console.WriteLine(result); // or save to disk
```
### DataTable
```csharp
var myDataTable as new DataTable()
// ... fill datatable with data here

var converter = new DataTableConverter(myDataTable);

string result = converter.ToXMLRecordset();

Console.WriteLine(result); // or save to disk
```

See [tests][CODE-EXAMPLES] for more examples.
## Why?

A challenge was given to me ! :wink:

A co-worker told me he wanted to send data from a `C#` web service into Excel spreadsheet (VBA I know !) through the network. Saving on disk is not allowed as it is a potential security risk.

The data should be able to be read using the [Recordset][MS-RecordsetADO]'s [`Open`][MS-RecordsetOpen] method to minimize changes to existing files.




  [MS-PRSTFR]: https://msdn.microsoft.com/en-us/library/cc313112%28v=office.12%29.aspx
  [MS-RecordsetADO]: https://docs.microsoft.com/en-us/sql/ado/reference/ado-api/recordset-object-ado?view=sql-server-2017
  [MS-RecordsetOpen]: https://docs.microsoft.com/en-us/sql/ado/reference/ado-api/open-method-ado-recordset?view=sql-server-2017
  [CODE-EXAMPLES]: https://gitlab.com/mdupuis13/Adonet2Adodb/tree/master/src/Adonet2Adodb_tests